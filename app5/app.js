// Richard Stewing 20.2.2013
//server with account db 


//loads modules
var fs=require('fs');
var http=require('http');
var io=require('socket.io');
var couchdb=require('felix-couchdb');
var cp=require('child_process');

//creates child process incase in takes longer in a child process
var dbcreate=cp.fork('dbcreate.js');

//read file
var pageHtml;
fs.readFile('namespace.html', function (err, data){
	if(err)
		throw err
	console.log(data.toString());
	pageHtml = data;
});

//db paramenters
var dbHost="127.0.0.1";
var dbPort=5984;
var dbName="users";


// sends the html file
var server=http.createServer();
server.on('request', function(req, res){
	//console.log(pageHtml.toString());
	res.writeHead(200, {'content-type':'text/html'});
	res.end(pageHtml);
	//console.log("N");
});


//listens to the server
var socket=io.listen(server);

//WS handler
socket.on('connection', function(connection){
	//connection
	console.log("connected uparunning");

	// sends news
	connection.emit('news',{contents:"serverconnection created"});

	// for account creation 
	connection.on('userdataput', function (data){
			createDoc(data, connection);
	});

	// for account data
	connection.on('userdataget', function (data){
		getDoc(data, connection);
	});
	
	
});

//creates doc
function createDoc(inf, conn){
	
	var client=couchdb.createClient(dbPort,dbHost);
	var user={
		names:{
			first: inf.first,
			last: inf.last
		}
	}
	var db= client.db(dbName);
	
	db.saveDoc(inf.username,user,function(err, doc){
		if(err){
			console.log(JSON.stringify(err));
			conn.emit("noData", {contents : "the username is already taken"})
		}else{
			console.log('saved user');
			console.log(doc);
		}
	});
}

//creates doc
function getDoc(inf, connection){

	var client=couchdb.createClient(dbPort,dbHost);
	var db= client.db(dbName);
	db.getDoc(inf.username, function (err, doc){
		console.log(doc);
		if(doc){
			connection.emit('ask',{contents: doc});
			connection.emit('news',{contents: doc});
		}else{
			connection.emit('noData',{contents: "it doesn't exist"});
		}
	});	
}
server.listen(8080);
