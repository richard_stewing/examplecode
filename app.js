var http = require('http');
var fs = require('fs');
var pageHtml;
fs.readFile('camera.html', function(err, data){
	if(err)
		throw err;
	console.log(data.toString());
	pageHtml = data.toString();

});

var server = http.createServer();

server.on('request', function(req, res){
	
	console.log(pageHtml.toString());
	res.writeHead(200, {'content-type':'text/html'});
	res.end(pageHtml);

});

server.listen(8080);