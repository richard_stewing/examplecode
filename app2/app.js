var fs=require('fs');
var http=require('http');
var io=require('socket.io');


var sockfile=fs.readFileSync('namespace.html');

var server=http.createServer();
server.on('request', function(req, res){
	res.writeHead(200, {'content-type':'text/html'});
	res.end(sockfile);
	console.log("http server connection");
});

var socket=io.listen(server);

socket.on('connection', function(upandrunning){
	console.log("socket io server connnection ");
	upandrunning.emit("message", {});
});

socket.of('/weather').on('connection', function(connection){
	console.log("weather connection");
	connection.emit("message", {});
	
});

server.listen(8080);


