//1.2.2013 by Richard Stewing 
// solves quadratic equations passed as a string form : x*x(+)px(+)q

var pq = function (equation){
	// create local varibels
	var p, q, d, that;

	// extracts p and q from the sring 
	p = sliceToP(equation);
	q = sliceToQ(equation);
	that = {};
	

	// creates d
	d = ((p/2)*(p/2))-q;

	// starts to give the return object "that" the props
	that.p = p;
	that.q = q;
	that.d = d;

	//using the pq formula rules 
	//if d ist smaller than 0
	if(d<0){
		that.solution = "doesn't exist";
		that.solutionCount = 0;
		console.log(that);
		return that;

	//if d is equal 0
	}else if(d === 0){
		that.solution = -1*(p/2);
		that.solutionCount = 1;
		console.log(that);
		return that;


	// if d is bigger than 0 
	}else {
		that.solution = {};
		that.solution.x1 = -1*(p/2)+Math.sqrt(d);
		that.solution.x2 = -1*(p/2)+(-1*Math.sqrt(d));
		that.solutionCount = 2;
		console.log(that);
		return that;
	}

};



var sliceToP = function (eq){
	var linPart, posTimes;
	//gives the linear part of the function 
	linPart = eq.slice(4);
	// finds position of the remaining x to extrect p
	posTimes = linPart.search("x");
	return parseInt(linPart.slice(0, posTimes));
};


var sliceToQ = function (eq){
	var linPart, posPlus;
	// gives linear part
	linPart = eq.slice(4);
	// finds position of the remaining x plus 2 to extrect q
	posPlus = linPart.search("x")+2;
	return parseInt(linPart.slice(posPlus));
};






